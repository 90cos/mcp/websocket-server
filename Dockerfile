FROM alpine:3.13

RUN apk update && apk add npm

WORKDIR /srv/node-app

COPY ./package.json .

RUN npm i

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
